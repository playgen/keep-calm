#/*F************************************************************************
# * openXBOW - the Passau Open-Source Crossmodal Bag-of-Words Toolkit
# * Copyright (C) 2016-2017, 
# *   Maximilian Schmitt & Bj�rn Schuller: University of Passau.
# *   Contact: maximilian.schmitt@uni-passau.de
# *  
# *  This program is free software: you can redistribute it and/or modify 
# *  it under the terms of the GNU General Public License as published by 
# *  the Free Software Foundation, either version 3 of the License, or 
# *  (at your option) any later version.
# *  
# *  This program is distributed in the hope that it will be useful, 
# *  but WITHOUT ANY WARRANTY; without even the implied warranty of 
# *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# *  GNU General Public License for more details.
# *  
# *  You should have received a copy of the GNU General Public License 
# *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ***********************************************************************E*/

h:Print this help\n

# IO
i:Name/Path of an input (ARFF or CSV) file p containing feature vectors over time\n\
  The first feature must be a string or number which specifies all feature vectors which belong to one analysis window/instance.
attributes:An optional string, specifying all input attributes (mandatory in case of multiple labels): \n\
           n=name, t=time stamp, 0=text feature, 1-9=numeric feature, c=class label/numeric label, r=remove attribute\n\
           Using different numbers for numeric features will create a separate codebook and bag for all features belonging to the same index.\n\
           The codebook index is followed by brackets [] specifying the number of consecutive input features belonging to this index.\n\
           Example: -attributes nt1[14]2[14]c\n\
           Input file with the structure: name,timestamp,28 numeric features split into two codebooks (14 features each) and one label.
o:Name / Path of an output ARFF, CSV or LibSVM file p containing the bag-of-words representation.\n\
  The file format depends on the given file ending (*.arff, *.csv or *.libsvm).
oi:Name / Path of an output CSV file p containing the word indexes.
svmModel:Name / Path of a Liblinear model (must be L2R_LR_DUAL) to decode the BoW.\n\
         p specifies the model file. openXBOW outputs a JSON file with the same name (unless given by oJson option).
oJson:Name / Path of the JSON output file including the predictions of the Liblinear model (must be given by the option svmModel).
writeName:Output the id string/number in the output file (only ARFF & CSV).
writeTimeStamp:Output the time stamp in the output file (only ARFF & CSV).
arffLabels:String containing all potential class labels (separator comma without whitespaces) for ARFF output file.\n\
           Only required if not all labels are found in the input data or (?=unknown). Example: -arffLabels class1,class2,class3
append:Append output to file (if output file already exists).
l:CSV file p with the class labels for each analysis window/instance.\n\
  In case a label file is given, the output is restricted to the instances, where labels are given.\n\
  Both nominal and numeric classes are supported. Format:\n\
  1st line (optional): name (according to the input file); label1; label2; ...\n\
  2nd line:            'file_1.wav'; class1; ...\n\
                       [and so on]\n

# Preprocessing options
t:Segment the input files with a windows size (segment width) of p1 seconds and a hop size (shift) of p2 seconds\n\
  If this option is used, the second column of the input file must be a time index (in seconds) of the current frame and \n\
  the (optional) labels file must have the corresponding time stamp as the 2nd column (name; time; label).\n
e:Remove all feature vectors from the input, where the activity (or energy) is below p2. Index p1 specifies the index of the activity attribute (first index: 1).
standardizeInput:Standardize all numeric input features.\n\
  The parameters are stored in the codebook file (-B) and then used for standardization of test data (-b) in an online approach.
normalizeInput:Normalize all numeric input features (min->max is normalized to 0->1).\n\
  The parameters are stored in the codebook file (-B) and then used for standardization of test data (-b) in an online approach.

# Codebook (only numeric features)
size:Set the (initial) size p of the codebook. (default: size=500)\n\
     In case of several codebooks (see -attributes) different sizes can be specified using separator comma, e.g., -size 200,500,100
c:Method of creating the codebook:\n\
  p=random (default): Generate the codebook by random sampling of the input feature vectors.\n\
  p=random++: Generate the codebook by random sampling of the input feature vectors with weighting similiar to initialization of kmeans++.\n\
  p=kmeans: Employ kmeans clustering (Lloyd's algorithm).\n\
  p=kmeans++: Employ kmeans++ clustering (Lloyd's algorithm).\n\
  p=generic: Generate a generic codebook (independent from data). The parameter "-size" is not relevant when selecting this method.
gen:Offset for the values in the generic codebook. 
reduce:Reduce the size of the codebook by merging words which are correlated with each other. PCC with threshold p is considered.
supervised:Generate a codebook for each class separately, first, then merge all codebooks. (Not available for numeric labels.)
seed:Select the random seed p used for codebook creation. (Has no effect on training selection configured by -numTrain).
numTrain:Randomly choose p feature vectors from the input data for the creation of the codebook (should not be used for random sampling).\n

unigram:Apply the n-gram approach to numeric features using unigrams. Only the p most frequent codewords are taken into account.
bigram:Apply the n-gram approach to numeric features using bigrams. The p most frequent codewords are taken into account.
trigram:Apply the n-gram approach to numeric features using trigrams. The p most frequent codewords are taken into account.\n\
        The uni-/bi-/trigram codebooks are stored in the codebook file (-B) and used when loading a codebook (-b).\n\
        In case of several codebooks (see -attributes) different sizes can be specified using separator comma, e.g., -bigram 200,600\n\
        In case of using uni-/bi-/trigrams, the standard BoW are no longer generated for the respective codebook.\n\
        p=0 results in the standard BoW approach. \n

b:Load codebook p (do not create one).
B:Save the created codebook as a file p.\n

# Assignment (only numeric)
a:When creating the bag-of-words, assign each input feature vector to p closest words from the codebook. (default: a=1, only closest word)\n\
  In case of several codebooks (see -attributes), a different number can be specified for each codebook using separator comma, e.g., -a 5,2\n\
  This parameter is stored in the codebook file (-B) and used when the respective codebook is loaded (-b).
gaussian:Soft assignment using Gaussian encoding with standard deviation (stddev) p.\n\
         In case of several codebooks (see -attributes), a different stddev can be specified for each codebook using separator comma, e.g., -a 25.0,30.0\n\
         This parameter is stored in the codebook file (-B) and used when the respective codebook is loaded (-b).
off:Off codebook words: Features with an Euclidean distance above threshold p to codewords are not be considered in the assignment step.\n\
    In case of several codebooks (see -attributes), a different stddev can be specified for each codebook using separator comma, e.g., -off 25.0,30.0\n\
    This parameter is stored in the codebook file (-B) and used when the respective codebook is loaded (-b).\n

# Assignment (only text)
minTermFreq:Gives a minimum threshold for the number of occurrences of each word/n-gram to be considered for codebook generation (default: minTermFreq=1)
maxTermFreq:Gives a maximum threshold for the number of occurrences of each word/n-gram to be considered for codebook generation (default: minTermFreq=0(inf))
stopChar:Specifies characters which are removed from all input instances (default: .,;:()?!* )
nGram:N-gram (default: nGram=1)
nCharGram:N-character-gram (default: nCharGram=0)\n

# Assignment general
log:Logarithmic term weighting 'lg(TF+1)' of the term frequency.\n\
    This parameter is stored in the codebook file (-B) and used when the respective codebook is loaded (-b).
idf:Inverse document frequency transform: Multiply the term frequency (TF) with the logarithm of the ratio of the \n\
    total number of instances and the number of instances where the respective word is present.\n\
    This parameter is stored in the codebook file (-B) and used when the respective codebook is loaded (-b).
norm:Normalize the bag-of-words (3 options of normalization).\n\
     p=1: Divides the term frequencies (TF) by the number of input frames.\n\
     p=2: Divides the TF by the sum of all TFs.\n\
     p=3: Divides the TF by a factor so that the resulting Euclidean length is 1.\n

# Postprocessing options
standardizeOutput:Standardize all output features (term frequencies).\n\
  The parameters are stored in the codebook file (-B) and then used for standardization of test data (-b) in an online approach.
normalizeOutput:Normalize all output features (term frequencies, min->max is normalized to 0->1).\n\
  The parameters are stored in the codebook file (-B) and then used for standardization of test data (-b) in an online approach.
