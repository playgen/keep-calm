﻿using PlayGen.Assets.SEWA.Analyser;
using PlayGen.Assets.SEWA.Analyser.AudioAnalyser;
using UnityEngine;

public class EmotionCheck : MonoBehaviour {

	private MicAnalyser _micAnalyser;
	private bool _micChecking;

	private void Awake()
	{
		var monobehaviourContext = this;
		var audioAnalyser = new CommandlineClient(monobehaviourContext);
		_micAnalyser = new MicAnalyser(5000, 5000, monobehaviourContext, audioAnalyser, OnAnalysisCompleted);
	}

	private void Update()
	{
		if (WorldControl.Instance.Playing)
		{
			if (!_micChecking)
			{
				_micAnalyser.StartAnalysis();
				_micChecking = true;
			}
			
		}
		else
		{
			if (_micChecking)
			{
				_micAnalyser.StopAnalysis();
				_micChecking = false;
			}
		}
	}

	private void OnDisable()
	{
		_micAnalyser.StopAnalysis();
	}

	public void OnAnalysisCompleted(AnalysisResult result)
	{
		var speedIncrease = 0f;
		if (result.Results["Boredom"] > 0.25f)
		{
			speedIncrease += 1 - result.Results["Boredom"];
		}
		if (result.Results["Positivity"] < 0.5f)
		{
			speedIncrease += result.Results["Positivity"];
		}
		if (result.Results["Sincerity"] < 0.5f)
		{
			speedIncrease += result.Results["Sincerity"];
		}
		if (result.Results["Stress"] > 0.25f)
		{
			speedIncrease += 1 - result.Results["Stress"];
		}
		if (result.Results["Volume"] < 0.25f)
		{
			speedIncrease += result.Results["Volume"];
		}
		speedIncrease *= 0.04f;
		PieceMovement.speed *= (1 + speedIncrease);
	}
}
