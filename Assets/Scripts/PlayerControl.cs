﻿using System.Collections;

using UnityEngine;

public class PlayerControl : MonoBehaviour {

	bool _moving;
	bool _grounded;
	int _position;
	Rigidbody _rigidbody;
	Animator _anim;

	private void Start()
	{
		_rigidbody = GetComponent<Rigidbody>();
		_anim = GetComponent<Animator>();
		_anim.Play("Run");
	}

	void Update () {
		if (WorldControl.Instance.Playing)
		{
			if (!_moving)
			{
				if (Input.GetKey(KeyCode.LeftArrow))
				{
					if (_position > -1)
					{
						_position--;
						StartCoroutine(Move());
					}
				}
				else if (Input.GetKey(KeyCode.RightArrow))
				{
					if (_position < 1)
					{
						_position++;
						StartCoroutine(Move());
					}
				}
				else
				{
					var currentPos = transform.position;
					transform.position = new Vector3(_position * 5f, currentPos.y, currentPos.z);
				}
			}
			if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				if (_grounded)
				{
					_rigidbody.AddForce(new Vector3(0, 7, 0), ForceMode.Impulse);
					_grounded = false;
					_anim.StopPlayback();
					_anim.Play("Running_Jump");
				}
			}
		}
	}

	IEnumerator Move()
	{
		_moving = true;
		var currentPos = transform.position;
		var newPos = new Vector3(_position * 5f, currentPos.y, currentPos.z);
		var timer = 0f;
		while (timer <= 1)
		{
			if (WorldControl.Instance.Playing)
			{
				timer += Time.smoothDeltaTime * 4;
				_rigidbody.MovePosition(Vector3.Lerp(currentPos, newPos, timer));
				yield return new WaitForEndOfFrame();
			}
			else
			{
				timer = 1.1f;
				yield return null;
			}
		}
		if (WorldControl.Instance.Playing)
		{
			transform.position = newPos;
		}
		_moving = false;
	}

	private void OnTriggerEnter(Collider collision)
	{
		if (collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
		{
			_grounded = true;
		}
		if (collision.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
		{
			WorldControl.Instance.EndGame();
			_anim.Play("Death_02");
		}
	}
}
