﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TextReader : MonoBehaviour {

	[SerializeField]
	private TextAsset _textAsset;
	private Text _textUI;
	private Image _textHider;
	private string[] _text;
	private int _currentLine;
	public static int CurrentLine;
	
	void Start () {
		CurrentLine = 0;
		_textUI = GetComponent<Text>();
		_textHider = GetComponentInParent<Mask>().GetComponent<Image>();
		_text = _textAsset.text.Split('\n');
		_textUI.text = _text[_currentLine];
		StartCoroutine(RevealText());
	}
	
	IEnumerator RevealText()
	{
		_textHider.fillAmount = 0;
		var timer = 0f;
		while (timer <= 1)
		{
			if (!WorldControl.Instance.Playing)
			{
				_textHider.fillAmount = 0;
				timer = 1.1f;
			}
			else
			{
				timer += Time.smoothDeltaTime * 0.2f;
				_textHider.fillAmount = timer;
			}
			yield return new WaitForEndOfFrame();
		}
		if (WorldControl.Instance.Playing)
		{
			_textHider.fillAmount = 0;
			_currentLine++;
			if (_currentLine > _text.Length)
			{
				_currentLine = 0;
			}
			_textUI.text = _text[_currentLine];
			StartCoroutine(RevealText());
		}
		CurrentLine = _currentLine;
	}
}
