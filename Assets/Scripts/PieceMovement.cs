﻿using UnityEngine;

public class PieceMovement : MonoBehaviour {

	public static float speed = 25;

	void Update () {
		if (WorldControl.Instance.Playing)
		{
			transform.Translate(0, 0, -Time.smoothDeltaTime * speed);
		}
	}
}
