﻿using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WorldControl : MonoBehaviour {

	public static WorldControl Instance;
	private bool _playing;
	[SerializeField]
	private List<GameObject> _worldPieces;
	[SerializeField]
	private GameObject _environmentPiece;
	[SerializeField]
	private GameObject _playerPrefab;
	private List<GameObject> _piecePool = new List<GameObject>();
	private List<GameObject> _environmentPool = new List<GameObject>();
	private float _firstLength;
	private float _firstEnvironmentLength;

	[SerializeField]
	private TextReader _textReader;
	[SerializeField]
	private GameObject _startUI;
	[SerializeField]
	private Text _endScore;
	[SerializeField]
	private GameObject _endUI;

	public bool Playing
	{
		get { return _playing; }
	}

	private void Awake()
	{
		Instance = this;
	}

	void Start () {
		PieceMovement.speed = 25;
		for (int i = 0; i < 10; i++)
		{
			CreateEnvironmentPiece();
		}
		for (int i = 0; i < 50; i++)
		{
			CreatePiece();
		}
		_startUI.SetActive(true);
	}

	public void StartGame()
	{
		_textReader.enabled = true;
		Instantiate(_playerPrefab);
		_playing = true;
		_startUI.SetActive(false);
	}

	public void EndGame()
	{
		_playing = false;
		_endUI.SetActive(true);
		_endScore.text = "You survived " + TextReader.CurrentLine + " lines";
	}

	public void Restart()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	void Update () {
		if (WorldControl.Instance.Playing)
		{
			if (_piecePool[0].transform.position.z + _firstLength < -25)
			{
				var pieceToRemove = _piecePool[0];
				_piecePool.RemoveAt(0);
				Destroy(pieceToRemove);
				CreatePiece();
			}
			if (_environmentPool[0].transform.position.z + _firstEnvironmentLength < -25)
			{
				var pieceToReplace = _environmentPool[0];
				_environmentPool.RemoveAt(0);
				var currentPosition = pieceToReplace.transform.position;
				pieceToReplace.transform.position = new Vector3(currentPosition.x, currentPosition.y, GetEnvironmentPiecePosition(pieceToReplace));
				_environmentPool.Add(pieceToReplace);
			}
#if UNITY_EDITOR
			if (Input.GetKeyDown(KeyCode.Insert))
			{
				Restart();
			}
#endif
		}
	}

	void CreatePiece()
	{
		var piece = _worldPieces[Random.Range(0, _worldPieces.Count)];
		var pieceSize = GetBounds(piece).size.z;
		var pos = 100 + (pieceSize * 0.5f);
		if (_piecePool.Count > 0)
		{
			var lastPieceCombinedBounds = GetBounds(_piecePool.Last());
			pos = _piecePool.Last().transform.position.z + (lastPieceCombinedBounds.size.z * 0.5f) + (pieceSize * 0.5f);
		}
		var newObj = Instantiate(piece, new Vector3(0, -0.75f, pos), Quaternion.Euler(Vector3.zero));
		pieceSize = GetBounds(newObj).size.z;
		if (_piecePool.Count > 0)
		{
			newObj.transform.position = new Vector3(0, -0.75f, _piecePool.Last().transform.position.z + (GetBounds(_piecePool.Last()).size.z * 0.5f) + (pieceSize * 0.5f));
		}
		newObj.transform.Translate(0, 0, -Time.smoothDeltaTime * PieceMovement.speed);
		_piecePool.Add(newObj);
		_firstLength = GetBounds(_piecePool.First()).size.z * 0.5f;
	}

	void CreateEnvironmentPiece()
	{
		var newObj = Instantiate(_environmentPiece, new Vector3(0, -0.75f, GetEnvironmentPiecePosition(_environmentPiece)), Quaternion.Euler(Vector3.zero));
		newObj.transform.position = new Vector3(0, -0.75f, GetEnvironmentPiecePosition(newObj));
		newObj.transform.Translate(0, 0, -Time.smoothDeltaTime * PieceMovement.speed);
		_environmentPool.Add(newObj);
		_firstEnvironmentLength = GetBounds(_environmentPool.First()).size.z * 0.5f;
	}

	float GetEnvironmentPiecePosition(GameObject piece)
	{
		var pieceSize = GetBounds(piece).size.z;
		var pos = -2.1f + (pieceSize * 0.5f);
		if (_environmentPool.Count > 0)
		{
			var lastPieceCombinedBounds = GetBounds(_environmentPool.Last());
			pos = _environmentPool.Last().transform.position.z + (lastPieceCombinedBounds.size.z * 0.5f) + (pieceSize * 0.5f);
		}
		return pos;
	}

	Bounds GetBounds(GameObject piece)
	{
		var combinedBounds = new Bounds();
		if (piece.GetComponentsInChildren<Renderer>().Length > 0)
		{
			if (piece.GetComponent<Renderer>())
			{
				combinedBounds = piece.GetComponent<Renderer>().bounds;
			}
			foreach (var childRenderer in piece.GetComponentsInChildren<Renderer>())
			{
				if (childRenderer != piece.GetComponent<Renderer>())
				{
					combinedBounds.Encapsulate(childRenderer.bounds);
				}
			}
		}
		if (piece.GetComponentsInChildren<Collider>().Length > 0)
		{
			if (piece.GetComponent<Collider>())
			{
				combinedBounds = piece.GetComponent<Collider>().bounds;
			}
			foreach (var childRenderer in piece.GetComponentsInChildren<Collider>())
			{
				if (childRenderer != piece.GetComponent<Collider>())
				{
					combinedBounds.Encapsulate(childRenderer.bounds);
				}
			}
		}
		return combinedBounds;
	}
}
